<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//product
Route::get('getProductAll', 'ApiProductController@getProductAll');

Route::get('getProductCRM', 'ApiProductController@getProductCRM');

Route::delete('product/delete/{id}', 'ApiProductController@deleteProduct');

Route::post('product/simpan', 'ApiProductController@store');

//category
Route::get('category', 'ApiCategoryController@getCategory');

Route::post('category/store', 'ApiCategoryController@store');

Route::delete('category/delete/{id}', 'ApiCategoryController@deleteCategory');

Route::get('category/edit/{id}', 'ApiCategoryController@edit');

Route::post('category/update/{id}', 'ApiCategoryController@update');

//auth
Route::post('postLogin', 'AuthController@login');


