<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

use Illuminate\Support\Facades\Validator; 

class ApiCategoryController extends Controller
{
    public function getCategory(){
        $category = Category::all();
        return response([
            'status' => true,
            'category' => $category
        ], 200);
    }

    public function deleteCategory($id){
        $del = Category::find($id);
        if($del->delete()){
        	return response([
	            'status' => true,
	            'message' => 'Hapus Data Category Berhasil'
        	], 200);
        }else{
        	return response([
	            'status' => false,
	            'message' => 'Gagal Hapus Data Category'
        	], 200);
        }
    }

    public function update($id, Request $request){
        $validasi = Validator::make($request->all(), [
            'name' => 'required|unique:category,name'
        ]);

        if($validasi->fails()){
            return response([ 'status' => false, 'validasi' => $validasi->errors() ]);
        }else{
            $category = Category::where('id',$id);

            $category->update(
                [
                    'name' => $request->name,
                ]
            );
            
            return response([
                'status' => true,
                'message' => 'Update Category Success'
            ], 200);
        }
    }

    public function store (Request $request){

        $validasi  = Validator::make($request->all(), [
            'name' => 'required|unique:category,name'
        ]);

        if($validasi->fails()){
            return response([ 'status' => false, 'validasi' => $validasi->errors()], 200);
        }else{
            $category = new Category;
            $category->name = $request->name;
          
            if($category->save()){
                return response([
                    'status' => true,
                    'message' => 'Create Category Success',
                    'data'   => $category
                ], 200);
            }else{
                return response([
                    'status' => false,
                    'message' => 'Gagal Create Category'
                ], 200);
            }
        }
    }

    public function edit($id){
        $category = Category::find($id);

        if($category == null){
            return response([
                'status' => false,
                'category' => []
            ], 200);
        }else{
            return response([
                'status' => true,
                'category' => $category
            ], 200);
        }
    }

}
