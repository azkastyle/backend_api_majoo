<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Role;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request){
        $user = \DB::table('users_groups')
        ->where('groups.id', 2)
        ->where('users.email', $request->email)
        ->join('users','users.id','users_groups.user_id')
        ->join('groups','groups.id', 'users_groups.master_akses_id')
        ->first();

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return response()->json([
                'error' => false,
                'message' => 'Login Sukses',
                'data' => $user
            ],200);
        }else{
             return response()->json(['error' => true, 'message' => 'Email atau Password Salah'],200);
        }
    }
}
