<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use App\Helpers\Upload;

use Illuminate\Support\Facades\Validator; 

class ApiProductController extends Controller
{
    public function getProductAll(){
        $product = Product::paginate(8);

        if($product == null){
            return response([
                'status' => false,
                'products' => []
            ], 200);
        }else{
            return response([
                'status' => true,
                'products' => $product
            ], 200);
        }
    }

    public function deleteProduct($id){
        $del = Product::find($id);
        if($del->delete()){
            return response([
                'status' => true,
                'message' => 'Hapus Data Product Berhasil'
            ], 200);
        }else{
            return response([
                'status' => false,
                'message' => 'Gagal Hapus Data Product'
            ], 200);
        }
    }

    public function getProductCRM(){
        $product = \DB::table('products')
        ->select('products.*', 'category.name as category')
        ->join('category','category.id','products.id_kategori')
        ->get();

        // $product = Product::all();

        if($product == null){
            return response([
                'status' => false,
                'products' => []
            ], 200);
        }else{
            return response([
                'status' => true,
                'products' => $product
            ], 200);
        }
    }

    public function store(Request $request){
        $params = $request->all();

        $validasi  = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        $data['name'] = trim($params['name']);
        $data['id_kategori'] = $params['id_kategori'];
        $data['price'] = $params['price'];
        $data['description'] = $params['description'];
        $data['stok'] = $params['stok'];
        
        if (isset($params['thumbnail'])) {
            $imageToStorage = Upload::image_save($params['thumbnail'], $params['name']);
            $data['thumbnail'] = $imageToStorage;
        }

        $save = Product::create($data);

        if($validasi->fails()){
            return response([ 'status' => false, 'validasi' => $validasi->errors()], 200);
        }else{
            if ($save) {
                return response([
                    'status' => true,
                    'products' => $data,
                    'message' =>'Sukses Create Products'
                ], 200);
            }else {
                return response([
                    'status' => false,
                    'message' =>'Gagal Create Products'
                ], 200);
            }
        }
        
       
    }

}
