<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    protected $primaryKey = 'id';
    protected $fillable = ['name','id_kategori','thumbnail', 'price', 'description', 'stok'];
}
