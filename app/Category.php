<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
